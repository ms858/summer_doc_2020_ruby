require 'minitest/autorun'
require 'minitest/pride'
require './card.rb'

# Part 1 - Create a class with the following guidelines
#
#   * create a new class called "Card" in a file called card.rb 
#   * card should require three arguments to be initalized (and should be readable):
#     - value
#     - suit
#     - color
#   * a method of the card called display_name that outputs the display name of the card e.g. 9 of Clubs

#   Remember to import your file into this test file using require "./card.rb"

# ==========================================================================================================================

# Part 2 - Create a method(standalone, not part of a class) that returns an array of a standard deck of cards using your class object
# A "standard" deck of playing cards consists of 52 Cards in each of the 4 suits of Spades, Hearts, Diamonds, and Clubs. 
# Each suit contains 13 cards: Ace, 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King.


# WRITE YOUR CODE HERE.  Name your method `populate_deck`.



# ==========================================================================================================================

class IfChallenge < MiniTest::Test

  ### Part One ###
  def test_one
    assert_raises(Exception) {Card.new('ace', 'black', 'spades')}
    assert_raises(Exception) {Card.new(value: 'ace')}
    assert Card.new(value: "Ace", color: "Black", suit: "Spades")
  end

  def test_two
    card = Card.new(value: "Ace", color: "Black", suit: "Spades")
    assert_equal "Ace", card.value
    assert_equal "Black", card.color
    assert_equal "Spades", card.suit
  end

  def test_three
    card = Card.new(value: "ace", color: "black", suit: "spades") #do not modify this line :)
    assert_equal "Ace of Spades", card.display_name

    card = Card.new(value: "9", color: "black", suit: "clubs") #do not modify this line :)
    assert_equal "9 of Clubs", card.display_name
  end
  ### end part one ###

  ### Part Two
  # def test_four
  #   deck = populate_deck
  #   assert_equal 52, deck.size
  # end

  # def test_five
  #   deck = populate_deck
  #   reds = deck.select{|card| card.color == "red"}
  #   blacks = deck.select{|card| card.color == "black"}
    
  #   assert_equal 26, blacks.size
  #   assert_equal 26, reds.size
  # end

  # def test_dead_mans_hand
  #   deck = populate_deck
  #   aces = deck.select{|card| card.value == "ace" && card.color == "black"}
  #   eights = deck.select{|card| card.value == "8" && card.color == "black"}

  #   display_names = []
  #   aces.each {|c| display_names << c.display_name}
  #   eights.each {|c| display_names << c.display_name}

  #   assert_equal 2, aces.size
  #   assert_equal 2, eights.size

  #   assert display_names.include?("Ace of Spades")
  #   assert display_names.include?("Ace of Clubs")
  #   assert display_names.include?("8 of Spades")
  #   assert display_names.include?("8 of Clubs")
  # end
   ### end part two ###

end
